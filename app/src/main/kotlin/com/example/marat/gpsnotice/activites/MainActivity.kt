package com.example.marat.gpsnotice.activites


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.LocationManager
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import com.example.marat.gpsnotice.services.LocationsService
import com.example.marat.gpsnotice.R
import com.example.marat.gpsnotice.toast
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity()
{

    private val TAG = "MainActivity"

    var interval:Long = 0
    var fastestInterval :Long = 0
    var chatID:String = ""
    //settings
    lateinit var mSettings: SharedPreferences
    val APP_PREFERENCES:String = "mysettings"
    var APP_PREFERENCES_INTERVAL = "INTERVAL"
    var APP_PREFERENCES_fastestInterval = "APP_PREFERENCES_fastestInterval"
    var APP_PREFERENCES_CHAD_ID:String = "CHAT_ID"


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        switchService.setOnCheckedChangeListener { compoundButton, isChecked ->

            if(checkGPS() && checkNetWork())
            {
                if (isChecked == true)
                {
                    if (intervalEditText.text.toString() != "")
                    {
                        val serviceIntent = Intent(this, LocationsService::class.java)
                        interval = intervalEditText.text.toString().toLong()
                        fastestInterval = fastestIntervalEditText.text.toString().toLong()
                        serviceIntent.putExtra("INTERVAL", interval)
                        serviceIntent.putExtra("FASTEST_INTERVAL", fastestInterval)
                        serviceIntent.putExtra("chatID", chatID)
                        startService(serviceIntent)
                        toast(getString(R.string.location_start))
                    }
                    else
                    {
                        switchService.isChecked = false
                        toast(getString(R.string.empty_interval))
                    }
                }
                else
                {

                    val serviceIntent = Intent(this, LocationsService::class.java)
                    stopService(serviceIntent)
                    toast(getString(R.string.location_stop))
                }
            }
            else
            {
                switchService.isChecked = false
            }
        }

        switchSendMap.setOnCheckedChangeListener { compoundButton, isChecked ->

            if(isChecked == true)
            {
                LocationsService.SEND_MAP = true
            }
            else
            {
                LocationsService.SEND_MAP = false
            }
        }

        switchClearChatID.setOnCheckedChangeListener { compoundButton, isChecked ->

            if(isChecked == true)
            {
                var editor = mSettings.edit()
                editor.putString(APP_PREFERENCES_CHAD_ID, "")
                editor.apply()
                toast("Забыто")
            }
            else
            {
                var registerActivity = Intent(this, RegisteredActivity::class.java)
                startActivity(registerActivity)
            }
        }

        switchRoute.setOnCheckedChangeListener { compoundButton, isChecked ->
            if(isChecked == true)
            {
                LocationsService.BUILD_ROUTE = true
            }
            else
            {
                LocationsService.BUILD_ROUTE = false
            }
        }

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun checkGPS():Boolean
    {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val gpsEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if(gpsEnable == false)
        {
            toast(getString(R.string.error_connection_gps))
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
        return gpsEnable
    }

    fun checkNetWork():Boolean
    {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        if (!(netInfo != null && netInfo!!.isConnectedOrConnecting))
        {
            toast(getString(R.string.error_connection_network))
        }
        return netInfo != null && netInfo.isConnectedOrConnecting;
    }

    override fun onPause()
    {
        super.onPause()
        saveSetting()
    }

    override fun onResume()
    {
        loadSetting()
        super.onResume()
    }

    fun saveSetting(){
//        var editor = mSettings.edit()
////        editor.putLong(APP_PREFERENCES_INTERVAL, interval)
////        editor.putLong(APP_PREFERENCES_fastestInterval, fastestInterval)
////        editor.putString(APP_PREFERENCES_CHAD_ID, "123816937")
//        editor.apply()
    }

    fun loadSetting()
    {
//        if (mSettings.contains(APP_PREFERENCES_INTERVAL))
//            intervalEditText.setText(mSettings.getLong(APP_PREFERENCES_INTERVAL, 0).toString())
//
//        if(mSettings.contains(APP_PREFERENCES_fastestInterval))
//            fastestIntervalEditText.setText(mSettings.getLong(APP_PREFERENCES_fastestInterval, 0).toString())
//
        if (mSettings.contains(APP_PREFERENCES_CHAD_ID))
        {
            if(mSettings.getString(APP_PREFERENCES_CHAD_ID, "") == "")
            {
                var registerActivity = Intent(this, RegisteredActivity::class.java)
                startActivity(registerActivity)
            }
            else
            {
                chatID = mSettings.getString(APP_PREFERENCES_CHAD_ID, "").toString()
            }
        }
        else
        {
            var registerActivity = Intent(this, RegisteredActivity::class.java)
            startActivity(registerActivity)
        }
    }
}

