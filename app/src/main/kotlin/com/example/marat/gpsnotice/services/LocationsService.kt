package com.example.marat.gpsnotice.services

import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import com.example.marat.gpsnotice.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import java.io.BufferedInputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection

/**
 * Created by MaRaT on 24.11.2016.
 */

open class LocationsService() :
        Service(),
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
{

    private val TAG = "LocationsService"
    private var INTERVAL: Long? = null
    private var FASTEST_INTERVAL: Long? = null
    private var chatID: String? = ""


    lateinit var mLocationRequest: LocationRequest
    lateinit var mGoogleApiClient: GoogleApiClient
    lateinit var mCurrentLocation: Location
    lateinit var mLastUpdateTime: String
    lateinit var line: String
    var mOldLocation: Location? = null


    lateinit var listPoints :MutableList<LatLng>

    companion  object
    {
        var SEND_MAP = false
        var BUILD_ROUTE = false
    }

    override fun onCreate()
    {
        super.onCreate()

        line = getString(R.string.line_feed)

        listPoints = mutableListOf()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()

    }

    override fun onDestroy()
    {
        mGoogleApiClient.disconnect()
        task("*Выключен*")
        super.onDestroy()

    }


    fun task(msg: String)
    {
        var task = MyLocationTask()
        task.execute(msg)
    }

    inner class MyLocationTask : AsyncTask<String, Void, Void>()
    {
        override fun doInBackground(vararg params: String?): Void?
        {
            var url_text = "https://api.telegram.org/bot" + getString(R.string.token) //+ "/sendMessage?chat_id=" + chatID + "&parse_mode=markdown&text="
            var message = params[0]
            if (message == "")
            {
                val latitude = params[1]
                val longitude = params[2]
                listPoints.add(LatLng(latitude!!.toDouble(), longitude!!.toDouble()))
                url_text += getSendLocation(latitude.toString(), longitude.toString())
            }
            else
            {
                message += line + getTime()
                url_text += getSendMessage(message.toString())
            }
            var url = URL(url_text)
            var urlConnection = url.openConnection() as HttpsURLConnection
            urlConnection.doInput = true
            var gin = BufferedInputStream(urlConnection.inputStream);
            urlConnection.disconnect();
            return null
        }

        fun getTime(): String
        {
            val calendar = Calendar.getInstance()
            val simpleDateFormat = SimpleDateFormat(
                    "dd MMMM yyyy HH:mm:ss", Locale.getDefault())
            val strDate = simpleDateFormat.format(calendar.time)
            return strDate
        }

        fun getSendLocation(latitude: String, longitude: String): String
        {
            if (SEND_MAP == false)
            {
                return getSendMessage("*Latitude   :* $latitude$line*Longitude:* $longitude")
            }
            else
            {
                return "/sendLocation?chat_id=$chatID&latitude=$latitude&longitude=$longitude"
            }
        }

        fun getSendMessage(message: String): String
        {
            return "/sendMessage?chat_id=$chatID&parse_mode=markdown&text=$message"
        }

        fun onProgressUpdate()
        {
        }

        fun onPostExecute()
        {
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int
    {
        INTERVAL = intent?.getLongExtra("INTERVAL", (1000 * 10).toLong())
        FASTEST_INTERVAL = intent?.getLongExtra("FASTEST_INTERVAL", (1000 * 5).toLong())
        chatID = intent?.getStringExtra("chatID")
        mGoogleApiClient.connect()
        task("*Запущен*")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onLocationChanged(location: Location?)
    {
        Log.i(TAG, location.toString())
        mCurrentLocation = location!!

        if (mOldLocation == null)
        {
            mOldLocation = mCurrentLocation
        }
        if (mCurrentLocation != null && !mCurrentLocation.equals(mOldLocation))
        {
            mOldLocation = mCurrentLocation

            var task = MyLocationTask()
            task.execute("", mCurrentLocation.latitude.toString(), mCurrentLocation.longitude.toString())
        }
    }

    override fun onBind(intent: Intent?): IBinder
    {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnected(p0: Bundle?)
    {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(p0: Int)
    {
    }

    override fun onConnectionFailed(p0: ConnectionResult)
    {

    }


    fun startLocationUpdates()
    {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = INTERVAL!!
        mLocationRequest.fastestInterval = FASTEST_INTERVAL!!
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        // Request location updates
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

}