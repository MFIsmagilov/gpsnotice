package com.example.marat.gpsnotice.activites

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.marat.gpsnotice.R
import com.example.marat.gpsnotice.activites.MainActivity
import com.example.marat.gpsnotice.toast
import kotlinx.android.synthetic.main.activity_registered.*
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONStringer
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.net.URI
import java.net.URL
import java.util.*
import javax.net.ssl.HttpsURLConnection

class RegisteredActivity : AppCompatActivity()
{

    //settings
    lateinit var mSettings: SharedPreferences
    val APP_PREFERENCES:String = "mysettings"
    var APP_PREFERENCES_CHAD_ID:String = "CHAT_ID"
    var chatID:String = ""
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registered)

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)

        var rand = Random()
        code.text = (rand.nextInt(9999) + 1000).toString()
        sendCode.setOnClickListener {
            intentMessageTelegram(code.text.toString())
            var task = MyGetUserTask()
            chatID = task.execute().get()
            var registerActivity = Intent(this, MainActivity::class.java)
            startActivity(registerActivity)
        }
    }


    inner class MyGetUserTask : AsyncTask<Void, Void, String>()
    {
        override fun doInBackground(vararg params: Void?):String
        {
            //тут надо по нормальному
            var chatID = ""

            while (chatID == "")
            {
                try
                {

                    var url_text = "https://api.telegram.org/bot" + getString(R.string.token) + "/getUpdates"
                    var url = URL(url_text)
                    var urlConnection = url.openConnection() as HttpsURLConnection
                    urlConnection.doInput = true
                    var gin = BufferedReader(urlConnection.inputStream.reader());
                    var inputLine: String = gin.readLine()
                    while (inputLine != null)
                    {
                        val partMessage = "\"text\":\"" + code.text + "\""
                        if (inputLine.contains(partMessage) == true)
                        {
                            var index = inputLine.indexOf("{\"id\":")
                            index += 6
                            while (inputLine[index] != ',')
                            {
                                chatID += inputLine[index]
                                index++
                            }
                        }
                        inputLine = gin.readLine()
                    }

                }catch (e:Exception){}
            }
            return chatID
        }
    }

    fun intentMessageTelegram(msg: String)
    {
        val appName = "org.telegram.messenger"
        val isAppInstalled = isAppAvailable(this, appName)
        if (isAppInstalled)
        {
            //val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=gpsnotice_bot"))
            val myIntent = Intent(Intent.ACTION_SEND, Uri.parse("https://telegram.me/gpsnotice_bot"))
            myIntent.type = "text/plain"
            myIntent.setPackage(appName)
            myIntent.putExtra(Intent.EXTRA_TEXT, msg)
            startActivity(Intent.createChooser(myIntent, "GPS"))
            //startActivity(myIntent)
        }
        else
        {
            toast(getString(R.string.error_telegram))
        }
    }

    fun isAppAvailable(context: Context, appName: String): Boolean
    {
        val pm = context.packageManager
        try
        {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES)
            return true
        }
        catch (e: PackageManager.NameNotFoundException)
        {
            return false
        }
    }

    override fun onPause()
    {
        var editor = mSettings.edit()
        editor.putString(APP_PREFERENCES_CHAD_ID, chatID)
        editor.apply()
        super.onPause()
    }
}
