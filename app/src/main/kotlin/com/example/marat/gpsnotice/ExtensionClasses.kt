package com.example.marat.gpsnotice

import android.support.v7.app.AppCompatActivity
import android.widget.Toast

/**
 * Created by MaRaT on 24.11.2016.
 */

fun AppCompatActivity.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}